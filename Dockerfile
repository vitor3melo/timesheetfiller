FROM openjdk:11

ARG PROFILE=dev
ARG FIREFOXDRIVER_VERSION=0.32.0
ARG FIREFOX_VERSION=100.0.2
ARG FIREFOX_BIN=/usr/bin/firefox
ARG GECKODRIVER_PATH=/app/bin/geckodriver
ARG JAVA_OPTS

ENV JAVA_OPTS=$JAVA_OPTS
ENV PROFILE=$PROFILE
ENV FIREFOX_BIN=$FIREFOX_BIN
ENV GECKODRIVER_PATH=$GECKODRIVER_PATH

RUN apt-get update && apt-get install libappindicator1 libasound2 libatk1.0-0 \
    libatk-bridge2.0-0 libcairo-gobject2 libgconf-2-4 libgtk-3-0 \
    libice6 libnspr4 libnss3 libsm6 libx11-xcb1 libxcomposite1 \ 
    libxcursor1 libxdamage1 libxfixes3 libxi6 libxinerama1 \
    libxrandr2 libxss1 libxt6 libxtst6 fonts-liberation libpci3 libgl1 libegl1 \
    wget -y
RUN mkdir -p /app/bin

RUN wget --no-verbose -O /tmp/firefox.tar.bz2 https://download-installer.cdn.mozilla.net/pub/firefox/releases/$FIREFOX_VERSION/linux-x86_64/en-US/firefox-$FIREFOX_VERSION.tar.bz2 \
    && bunzip2 /tmp/firefox.tar.bz2 \
    && tar xvf /tmp/firefox.tar \
    && mv /firefox /opt/firefox-$FIREFOX_VERSION \
    && ln -s /opt/firefox-$FIREFOX_VERSION/firefox /usr/bin/firefox

RUN wget https://github.com/mozilla/geckodriver/releases/download/v$FIREFOXDRIVER_VERSION/geckodriver-v$FIREFOXDRIVER_VERSION-linux64.tar.gz \
    && tar -xf geckodriver-v$FIREFOXDRIVER_VERSION-linux64.tar.gz \
    && cp geckodriver ${GECKODRIVER_PATH} && rm geckodriver-v$FIREFOXDRIVER_VERSION-linux64.tar.gz && rm -rf geckodriver
RUN chmod +x ${GECKODRIVER_PATH}

COPY . /app
WORKDIR /app
RUN ./mvnw clean package
RUN cp ./target/timesheetfiller-0.0.1-SNAPSHOT.jar timesheetfiller.jar
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -Dspring.profiles.active=$PROFILE -Dwebdriver.gecko.driver=$GECKODRIVER_PATH -jar timesheetfiller.jar
