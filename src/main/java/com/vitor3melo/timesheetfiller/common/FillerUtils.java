package com.vitor3melo.timesheetfiller.common;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class FillerUtils {

    private FillerUtils() {
    }

    public static List<LocalDate> getCurrentWorkWeek() {
        LocalDate today = LocalDate.now();
        DayOfWeek todayDayOfWeek = today.getDayOfWeek();
        LocalDate weekStart = today.minusDays(todayDayOfWeek.getValue() - 1L);
        // from monday to saturday, 5 days
        LocalDate weekEnd = weekStart.plusDays(5);
        // check if penultimate week
        LocalDate nextSaturday = weekEnd.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
        var workWeek = weekStart.datesUntil(weekEnd).collect(Collectors.toList());
        if (nextSaturday.getMonth() != today.getMonth()) {
            LocalDate endOfMonth = today.with(TemporalAdjusters.lastDayOfMonth());
            while (endOfMonth.getDayOfWeek() != DayOfWeek.SUNDAY) {
                workWeek.add(endOfMonth);
                endOfMonth = endOfMonth.minusDays(1);
            }
        }
        return workWeek.stream().sorted().collect(Collectors.toList());
    }

    public static List<LocalDate> getPreviousWorkDays() {
        LocalDate today = LocalDate.now();
        LocalDate startOfMonth = today.with(TemporalAdjusters.firstDayOfMonth());
        LocalDate previousLastWorkDay = today.with(TemporalAdjusters.previous(DayOfWeek.SATURDAY));
        if (startOfMonth.isBefore(previousLastWorkDay)) {
            List<LocalDate> previousWorkDays = startOfMonth.datesUntil(previousLastWorkDay)
                    .filter(d -> d.getDayOfWeek() != DayOfWeek.SUNDAY && d.getDayOfWeek() != DayOfWeek.SATURDAY)
                    .collect(Collectors.toList());
            return previousWorkDays;
        }
        return Collections.emptyList();
    }

    public static List<LocalDate> getDaysBetween(LocalDate from, LocalDate to) {
        List<LocalDate> week = from.datesUntil(to).collect(Collectors.toList());
        week.add(to);
        return week;
    }

}
