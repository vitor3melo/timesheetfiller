package com.vitor3melo.timesheetfiller.configuration;

import java.nio.file.Path;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.annotation.EnableScheduling;

import lombok.extern.log4j.Log4j2;

@Configuration
@EnableScheduling
@Log4j2
@Profile("!dev")
public class TimesheetFillerConfiguration {

    @Value("${path.firefox}")
    String binaryPath;

    @Value("${path.geckodriver}")
    String geckoPath;

    @Value("${report.email.from}")
    String email;

    @Value("${report.email.password}")
    String password;

    @Value("${report.email.server}")
    String server;

    @Bean(destroyMethod = "quit")
    WebDriver webDriver() {
        System.setProperty("webdriver.gecko.driver", geckoPath);
        FirefoxOptions options = new FirefoxOptions();
        log.info(binaryPath);
        log.info(geckoPath);
        options.setBinary(Path.of(binaryPath));
        options.setHeadless(true);
        options.addPreference("javascript.options.mem.max", 134217728);
        options.addPreference("devtools.cache.disabled", true);
        return new FirefoxDriver(options);
    }

    @Bean
    JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(server);
        mailSender.setPort(587);

        mailSender.setUsername(email);
        mailSender.setPassword(password);

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");

        return mailSender;
    }

}
