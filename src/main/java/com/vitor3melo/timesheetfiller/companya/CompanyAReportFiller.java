package com.vitor3melo.timesheetfiller.companya;

import java.util.List;

import com.vitor3melo.timesheetfiller.exception.CouldNotFindElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.locators.RelativeLocator;
import org.openqa.selenium.support.ui.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class CompanyAReportFiller {

    private String dashboard;

    @Value("${report.field.text.main}")
    private String text;

    @Value("${report.field.text.alternative}")
    private String alternativeText;

    @Autowired
    WebDriver webDriver;

    public void fillReports(String dashboardUrl) {
        this.dashboard = dashboardUrl;
        WebElement selectedTable;
        do {
            selectedTable = selectWeek();
            if (selectedTable != null) {
                String weekText = getWeek(selectedTable).replaceAll("[A-Z][a-z]* |\\(|\\)", "");
                log.info("Filling week {}", weekText);
                if (weekText == null) {
                    throw new CouldNotFindElementException("Week is null");
                }
                clickCreateReportButton(selectedTable);
                fillReport(weekText);
            }
        } while (selectedTable != null);
    }

    private String getWeek(WebElement table) {
        By titleRelative = RelativeLocator.with(By.tagName("h5")).above(table);
        WebElement titleElement = webDriver.findElement(titleRelative);
        return titleElement.getText();
    }

    private WebElement selectWeek() {
        List<WebElement> tables = webDriver.findElements(By.cssSelector("table > tbody:nth-child(2)"));
        for (WebElement table : tables) {
            WebElement lastColumn = table.findElement(By.cssSelector("td:nth-child(5)"));
            if (!lastColumn.getText().toLowerCase().contains("missing")) {
                continue;
            }
            return table;
        }
        return null;
    }

    private void clickCreateReportButton(WebElement table) {
        WebElement createReportButton = table.findElement(By.cssSelector("td:nth-child(2) > button"));
        createReportButton.click();
        try {
            // sleep 3 seconds so the DOM updates
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            log.error("Thread error ");
            Thread.currentThread().interrupt();
        }
    }

    private void fillReport(String week) {
        Select selectWeek = new Select(webDriver.findElement(By.id("report_period")));
        selectWeek.selectByValue(week);

        List<WebElement> stars = webDriver.findElements(By.tagName("label"));
        for (WebElement label : stars) {
            if (label.getDomProperty("title").equalsIgnoreCase("amazing")) {
                label.click();
            }
        }

        String thisText = this.text;
        String nextText = this.alternativeText;

        if (Integer.parseInt(week.split("/")[0]) % 2 == 0) {
            thisText = this.alternativeText;
            nextText = this.text;
        }

        WebDriver iframeDriverThisWeek = webDriver.switchTo()
                .frame(webDriver.findElement(By.xpath("//iframe[contains(@title,'editor1')]")));
        WebElement editorThisWeek = iframeDriverThisWeek.findElement(By.tagName("body"));
        editorThisWeek.sendKeys(thisText);

        webDriver = webDriver.switchTo().defaultContent();
        
        WebDriver iframeDriverNextWeek = webDriver.switchTo()
                .frame(webDriver.findElement(By.xpath("//iframe[contains(@title,'editor2')]")));
        WebElement editorNextWeek = iframeDriverNextWeek.findElement(By.tagName("body"));
        editorNextWeek.sendKeys(nextText);

        webDriver = webDriver.switchTo().defaultContent();

        WebElement submitButton = webDriver.findElement(By.id("submit_btn"));
        submitButton.click();

        webDriver.get(this.dashboard);
        try {
            // sleep 3 seconds so the DOM updates
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            log.error("Thread error ");
            Thread.currentThread().interrupt();
        }

    }

}
