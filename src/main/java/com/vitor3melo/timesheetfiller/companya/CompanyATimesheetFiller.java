package com.vitor3melo.timesheetfiller.companya;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vitor3melo.timesheetfiller.exception.CouldNotFindElementException;
import com.vitor3melo.timesheetfiller.service.DateProviderService;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class CompanyATimesheetFiller {

    @Autowired
    private WebDriver webDriver;

    @Autowired
    private DateProviderService dateProviderService;

    private String workingProject;

    private static final String THREAD_ERROR_MESSAGE = "Thread was interrupted while in companya timesheet from {} to {}";
    private static final String DASHBOARD_ERROR_MESSAGE = "Thread was interrupted while in companya timesheet dashboard";
    private static final String TABLE_XPATH = "tbody/tr";
    private static final String DATE_PARSE_FORMAT = "LLL d, yyyy";

    public void navigateAndFill(String workingProject) {
        this.workingProject = workingProject;
        if (navigateToTimesheetDashboard()) {
            completeCalendar();
            openAndFillTimesheetByLine();
        }
    }

    public boolean navigateToTimesheetDashboard() {
        try {
            // sleep 5 seconds so the DOM updates
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            log.error(DASHBOARD_ERROR_MESSAGE);
            Thread.currentThread().interrupt();
        }
        webDriver.get("https://icehrm.com/app/companya/?g=modules&n=time_sheets&m=module_Time_Management");

        try {
            // sleep 5 seconds so the DOM updates
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            log.error(DASHBOARD_ERROR_MESSAGE);
            Thread.currentThread().interrupt();
        }
        return true;
    }

    private void completeCalendar() {
        WebElement table = webDriver.findElement(By.cssSelector("#grid"));
        List<WebElement> tableLines = table.findElements(By.xpath(TABLE_XPATH));

        List<LocalDate> startDates = new ArrayList<>();

        for (WebElement line : tableLines) {
            String date = line.findElement(By.xpath("td[1]")).getText();
            LocalDate parsedDate = parseDate(date, DATE_PARSE_FORMAT);
            log.info("Parsed table start date {}", parsedDate);
            startDates.add(parsedDate);
        }

        for (int i = 0; i < startDates.size() - 1; i++) {
            LocalDate current = startDates.get(i);
            LocalDate next = startDates.get(i + 1);
            LocalDate theoricalNext = current.with(TemporalAdjusters.previous(DayOfWeek.SUNDAY));
            log.info("Theorical previous start date {} current previous {}", theoricalNext, next);
            if (!theoricalNext.isEqual(next)) {
                log.info("There's no start date of the previous week. Should be {} found {}", theoricalNext, next);
                WebElement currentDateLine = tableLines.get(i);
                WebElement button = currentDateLine
                        .findElement(By.cssSelector("td:nth-child(5) > div:nth-child(1) > img:nth-child(3)"));
                log.info("Clicking to create the previous week");
                button.click();
                try {
                    // sleep 5 seconds so the DOM updates
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    log.error(DASHBOARD_ERROR_MESSAGE);
                    Thread.currentThread().interrupt();
                }
                completeCalendar();
                break;
            }
        }

    }

    public void openAndFillTimesheetByLine() {
        boolean updated = false;
        WebElement table = webDriver.findElement(By.cssSelector("#grid"));
        List<WebElement> tableLines = table.findElements(By.xpath(TABLE_XPATH));

        for (WebElement line : tableLines) {
            WebElement totalTime = line.findElement(By.cssSelector("td:nth-child(3)"));
            WebElement status = line.findElement(By.cssSelector("td:nth-child(4)"));
            if (totalTime.getText().equals("00:00") || status.getText().equalsIgnoreCase("rejected")) {

                WebElement startDateElement = line.findElement(By.cssSelector("td:nth-child(1)"));
                WebElement endDateElement = line.findElement(By.cssSelector("td:nth-child(2)"));

                LocalDate from = parseDate(startDateElement.getText(), DATE_PARSE_FORMAT);
                LocalDate to = parseDate(endDateElement.getText(), DATE_PARSE_FORMAT);
                log.info("Trying to fill from {} to {}", from, to);

                WebElement button = line
                        .findElement(By.cssSelector("td:nth-child(5) > div:nth-child(1) > img:nth-child(2)"));
                log.info("Clicking to open the timesheet");
                button.click();

                try {
                    // sleep 5 seconds so the DOM updates
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    log.error(DASHBOARD_ERROR_MESSAGE);
                    Thread.currentThread().interrupt();
                }

                updated = fillTimesheet(from, to);
                if (updated) {
                    break;
                }

            }
        }
        if (updated) {
            openAndFillTimesheetByLine();
        }
    }

    public boolean fillTimesheet(LocalDate from, LocalDate to) {

        List<LocalDate> workDays = dateProviderService.getWorkDays(from, to);
        log.info("Work days from {} to {} - {}", from, to, Arrays.toString(workDays.toArray()));
        List<LocalDate> vacationDays = dateProviderService.getVacationDays(from, to);
        log.info("Vacation days from {} to {} - {}", from, to, Arrays.toString(vacationDays.toArray()));

        WebElement timesheet = webDriver.findElement(By.id("Qtsheet"));
        WebElement timesheetTable = timesheet.findElement(By.tagName("table"));
        List<WebElement> header = timesheetTable.findElements(By.tagName("th"));
        List<WebElement> lines = timesheetTable.findElements(By.xpath(TABLE_XPATH));

        List<String> dates = header.stream().map(WebElement::getText).collect(Collectors.toList());

        WebElement projectLine = lines.stream()
                .filter(l -> l.getText().toLowerCase().contains(this.workingProject))
                .findAny().orElse(null);

        WebElement vacationLine = lines.stream()
                .filter(l -> l.getText().toLowerCase().contains("vacation"))
                .findAny().orElse(null);

        if (projectLine == null) {
            throw new CouldNotFindElementException("Project not found, aborting operation");
        }

        if (vacationLine == null) {
            throw new CouldNotFindElementException("vacationLine is null");
        }

        for (LocalDate workDay : workDays) {
            log.info("Filling workday {}", workDay);
            int rowPosition = getRowPosition(workDay, dates);
            WebElement cell = projectLine
                    .findElement(By.cssSelector("td:nth-child(" + (rowPosition + 1) + ")"));
            cell.sendKeys(Keys.ENTER + "8");
        }

        for (LocalDate vacationDay : vacationDays) {
            log.info("Filling vacation day {}", vacationDay);
            int rowPosition = getRowPosition(vacationDay, dates);
            WebElement cell = vacationLine
                    .findElement(By.cssSelector("td:nth-child(" + (rowPosition + 1) + ")"));
            cell.sendKeys(Keys.ENTER + "8");
        }

        WebElement saveButton = webDriver.findElement(By.cssSelector(".saveBtnTable"));
        log.info("Clicking save button");
        saveButton.click();

        try {
            // sleep 5 seconds so the DOM updates
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            log.error(THREAD_ERROR_MESSAGE, from, to);
            Thread.currentThread().interrupt();
        }

        WebElement submitButton = webDriver.findElement(By.cssSelector(".completeBtnTable"));
        log.info("Clicking submit button");
        submitButton.click();

        try {
            // sleep 5 seconds so the DOM updates
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            log.error(THREAD_ERROR_MESSAGE, from, to);
            Thread.currentThread().interrupt();
        }

        WebElement cancelButton = webDriver.findElement(By.cssSelector(".cancelBtnTable"));
        log.info("Going back to the timesheet dashboard");
        cancelButton.click();

        try {
            // sleep 5 seconds so the DOM updates
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            log.error(THREAD_ERROR_MESSAGE, from, to);
            Thread.currentThread().interrupt();
        }

        return true;
    }

    private LocalDate parseDate(String date, String pattern) {
        date = date.replaceAll("\\([A-Za-z]*\\)", "").trim();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return LocalDate.parse(date, formatter);
    }

    private int getRowPosition(LocalDate date, List<String> dates) {
        String dateString = date.format(DateTimeFormatter.ofPattern("dd LLL"));
        for (int i = 0; i < dates.size(); i++) {
            if (dates.get(i).replaceAll("\\([A-Za-z]*\\)", "").trim().equals(dateString)) {
                return i;
            }
        }
        return -1;
    }

}
