package com.vitor3melo.timesheetfiller.companya;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class CompanyALoginFiller {

    @Autowired
    WebDriver webDriver;

    public void fillLogin(String loginUrl, String username, String password) {
        log.info("Calling companya login page");
        webDriver.manage().window().maximize();
        webDriver.get(loginUrl);

        try {
            // sleep 2 seconds so the DOM updates
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            log.error("Thread was interrupted while fetching companya login");
            Thread.currentThread().interrupt();
        }

        try {
            log.info("Checking if login page was reached");
            var isLoginPage = new WebDriverWait(this.webDriver, Duration.ofSeconds(2))
                    .until(driver -> driver.getCurrentUrl().contains("login"));
            if (isLoginPage != null && isLoginPage) {
                WebElement form = webDriver.findElement(By.id("loginForm"));
                log.info("Filling login form");
                WebElement usernameInput = form.findElement(By.id("username"));
                WebElement passwordInput = form.findElement(By.id("password"));
                log.info("Filling username");
                usernameInput.sendKeys(username);
                log.info("Filling password");
                passwordInput.sendKeys(password);
                log.info("Clicking login");
                form.submit();
            }
        } catch (TimeoutException e) {
            log.info("Login page was not reached");
        }
    }

}
