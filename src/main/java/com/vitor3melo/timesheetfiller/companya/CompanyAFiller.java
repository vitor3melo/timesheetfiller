package com.vitor3melo.timesheetfiller.companya;

import com.vitor3melo.timesheetfiller.common.Filler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class CompanyAFiller implements Filler {

    @Autowired
    private CompanyALoginFiller loginFiller;

    @Autowired
    private CompanyATimesheetFiller timesheetFiller;

    @Autowired
    private CompanyAReportLoginFiller reportLoginFiller;

    @Autowired
    private CompanyAReportFiller reportFiller;

    @Value("${url.login.companya}")
    private String loginUrl;

    @Value("${credentials.companya.username}")
    private String username;

    @Value("${credentials.companya.password}")
    private String password;

    @Value("${project.companya}")
    private String workingProject;

    @Value("${url.reports.companya.dashboard}")
    private String companyaReportsDashboard;

    @Value("${url.reports.companya.login}")
    private String companyaReportsLogin;

    private int numberOfRuns = 0;

    @Override
    public void run() {
        try {
            numberOfRuns++;
            log.info("Logging in companya");
            loginFiller.fillLogin(loginUrl, username, password);
            log.info("Starting timesheet filling in Cleveri");
            timesheetFiller.navigateAndFill(workingProject);
            log.info("Starting reports login in Cleveri");
            reportLoginFiller.fillLogin(companyaReportsLogin, username, password);
            log.info("Starting report filling in Cleveri");
            reportFiller.fillReports(companyaReportsDashboard);
        } catch (Exception e) {
            log.error(e);
            if(numberOfRuns < 2) {
                log.info("Retrying run");
                this.run();
            }
        }
    }

}
