package com.vitor3melo.timesheetfiller.companya;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class CompanyAReportLoginFiller {

    @Autowired
    WebDriver webDriver;

    public void fillLogin(String loginUrl, String username, String password) {
        log.info("Calling companya Reports login page");
        webDriver.get(loginUrl);

        try {
            log.info("Checking if login page was reached");
            var isLoginPage = new WebDriverWait(this.webDriver, Duration.ofSeconds(2))
                    .until(driver -> driver.getCurrentUrl().contains("login"));
            if (isLoginPage != null && isLoginPage) {
                log.info("Filling login form");
                WebElement usernameElement = webDriver.findElement(By.id("user_email"));
                WebElement passwordElement = webDriver.findElement(By.id("user_password"));
                usernameElement.sendKeys(username);
                passwordElement.sendKeys(password);
                log.info("Clicking login");
                WebElement form = webDriver.findElement(By.tagName("form"));
                form.submit();
                threadWait();
            }
        } catch (TimeoutException e) {
            log.info("Login page was not reached");
        }
    }

    private void threadWait() {
        try {
            // sleep 3 seconds so the DOM updates
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            log.error("Thread error ");
            Thread.currentThread().interrupt();
        }
    }

}
