package com.vitor3melo.timesheetfiller.exception;

public class CouldNotFindProjectException extends RuntimeException {
    
    public CouldNotFindProjectException(String message) {
        super(message);
    }

}
