package com.vitor3melo.timesheetfiller.exception;

public class CouldNotGetURLDateException extends RuntimeException {

    public CouldNotGetURLDateException(String message) {
        super(message);
    }
}
