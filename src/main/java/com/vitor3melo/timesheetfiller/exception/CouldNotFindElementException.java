package com.vitor3melo.timesheetfiller.exception;

public class CouldNotFindElementException extends RuntimeException {

    public CouldNotFindElementException(String message) {
        super(message);
    }

}
