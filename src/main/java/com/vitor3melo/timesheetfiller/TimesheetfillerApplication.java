package com.vitor3melo.timesheetfiller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy
public class TimesheetfillerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TimesheetfillerApplication.class, args);
	}

}
