package com.vitor3melo.timesheetfiller.aspects;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.vitor3melo.timesheetfiller.service.EmailSenderService;

import lombok.extern.log4j.Log4j2;

@Profile("!dev")
@Log4j2
@Component
@Aspect
public class ErrorHandlingAspect {

    @Autowired
    WebDriver webDriver;

    @Autowired
    EmailSenderService emailSenderService;

    @AfterThrowing(value = "execution(* com.vitor3melo..*(..))", throwing = "e")
    public void handleNoSuchElementException(NoSuchElementException e) {
        File screenshotFile = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(screenshotFile, new File("./screenshots/failed.png"));
            emailSenderService.sendEmail("Error logging in",
                    ExceptionUtils.getStackTrace(e), "./screenshots/failed.png");
        } catch (IOException e1) {
            log.error("Could not save snapshot", e1);
        }
    }

}
