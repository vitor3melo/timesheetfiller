package com.vitor3melo.timesheetfiller.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class Holiday {

    @NonNull
    @JsonProperty("name")
    private String title;

    @NonNull
    @JsonProperty("date") Date date;

}
