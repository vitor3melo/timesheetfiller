package com.vitor3melo.timesheetfiller.service.dto;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Date {

    @JsonProperty("iso")
    private LocalDate iso;

}
