package com.vitor3melo.timesheetfiller.service;

import java.io.File;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class EmailSenderService {

    @Value("${report.email.from:}")
    private String fromEmail;

    @Value("${report.email.to:}")
    private String toEmail;

    @Autowired
    private JavaMailSender emailSender;

    public void sendEmail(String subject, String stacktrace, String attachmentPath) {
        MimeMessage message = emailSender.createMimeMessage();

        MimeMessageHelper helper;
        try {
            helper = new MimeMessageHelper(message, true);
            helper.setFrom(fromEmail);
            helper.setTo(toEmail);
            helper.setSubject(subject);
            helper.setText("Something went wrong. I was looking at this and it gave this error: " + stacktrace);
            FileSystemResource file = new FileSystemResource(new File(attachmentPath));
            helper.addAttachment("screenshot.png", file);
        } catch (MessagingException e) {
            log.error("Error sending the message", e);
        }

        emailSender.send(message);
    }

}
