package com.vitor3melo.timesheetfiller.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.vitor3melo.timesheetfiller.companya.CompanyAFiller;
import com.vitor3melo.timesheetfiller.companyb.replicon.CompanyBMainOrchestrator;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class RoutineService {

    @Autowired
    private CompanyBMainOrchestrator companybFiller;

    @Autowired
    private CompanyAFiller companyaFiller;

    @Autowired
    private ShutdownManager shutdownManager;

    @EventListener(ApplicationReadyEvent.class)
    void runRoutine() {
        companybFiller.run();
        companyaFiller.run();
        log.info("Routine completed - gracefully shutting down");
        shutdownManager.initiateShutdown(0);
    }

}
