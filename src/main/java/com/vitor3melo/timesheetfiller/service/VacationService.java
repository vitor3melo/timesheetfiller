package com.vitor3melo.timesheetfiller.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.stereotype.Component;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class VacationService {

    private List<LocalDate> vacationCache;

    public List<LocalDate> getVacations() {
        if (vacationCache != null) {
            return vacationCache;
        } else {
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader("vacations.txt"))) {
                vacationCache = bufferedReader.lines().map(line -> line.split(",")).flatMap(dates -> {
                    if (dates.length == 2) {
                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
                        LocalDate start = LocalDate.parse(dates[0].trim(), formatter);
                        LocalDate end = LocalDate.parse(dates[1].trim(), formatter);
                        return Stream.concat(start.datesUntil(end), Stream.of(end));
                    }
                    return Stream.of();
                }).collect(Collectors.toList());
                return vacationCache;
            } catch (IOException e) {
                log.error(e.getMessage());
                return new ArrayList<>();
            }
        }

    }

    public List<LocalDate> removeVacations(List<LocalDate> days) {
        List<LocalDate> vacations = getVacations();
        log.info("Current vacation days: {}", Arrays.toString(vacations.toArray()));
        return days.stream().filter(d -> !vacations.contains(d)).collect(Collectors.toList());
    }

    public List<LocalDate> getVacations(LocalDate from, LocalDate to) {
        List<LocalDate> vacations = getVacations();
        var filteredVacations = vacations.stream().filter(d -> (d.isEqual(from) || d.isAfter(from)) && (d.isEqual(to) || d.isBefore(to))).sorted()
                .collect(Collectors.toList());
        return filteredVacations;
    }

}
