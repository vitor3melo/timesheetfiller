package com.vitor3melo.timesheetfiller.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import com.vitor3melo.timesheetfiller.service.dto.HolidayDto;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class HolidayService {

    @Value("${holidays.url}")
    private String holidaysUrl;

    @Value("${holidays.api-key}")
    private String key;

    @Value("${holidays.country}")
    private String country;

    private static String filename = "holidays.txt";

    public List<LocalDate> getHolidays() {

        File holidaysFile = new File(filename);
        if (holidaysFile.exists()) {
            try (BufferedReader reader = new BufferedReader(new FileReader(holidaysFile))) {
                return reader.lines().map(LocalDate::parse).collect(Collectors.toList());
            } catch (IOException e) {
                log.error(e.getMessage());
            }
        }

        HolidayDto dto = WebClient.create(holidaysUrl).get()
                .uri(t -> t.queryParam("api_key", key).queryParam("country", country)
                        .queryParam("year", LocalDate.now().getYear()).queryParam("type", "national").build())
                .accept(MediaType.APPLICATION_JSON).retrieve().bodyToMono(HolidayDto.class).block();

        if (dto == null || dto.getResponse() == null || dto.getResponse().getHolidays() == null
                || dto.getResponse().getHolidays().isEmpty()) {
            return Collections.emptyList();
        }

        List<LocalDate> holidayDates = dto.getResponse().getHolidays().stream().map(h -> h.getDate().getIso())
                .collect(Collectors.toList());

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename))) {
            for (LocalDate date : holidayDates) {
                writer.write(date.toString());
                writer.newLine();
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        }

        return holidayDates;
    }

    public List<LocalDate> removeHolidays(List<LocalDate> days) {
        List<LocalDate> holidays = getHolidays();
        return days.stream().filter(d -> !holidays.contains(d)).collect(Collectors.toList());
    }

}
