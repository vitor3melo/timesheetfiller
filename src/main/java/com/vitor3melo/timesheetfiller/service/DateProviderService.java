package com.vitor3melo.timesheetfiller.service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.vitor3melo.timesheetfiller.common.FillerUtils;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class DateProviderService {

    @Autowired
    private HolidayService holidayService;

    @Autowired
    private VacationService vacationService;

    public List<LocalDate> getCurrentWeekDays() {
        List<LocalDate> currentWeekDays = FillerUtils.getCurrentWorkWeek();
        currentWeekDays = holidayService.removeHolidays(currentWeekDays);
        currentWeekDays = vacationService.removeVacations(currentWeekDays);
        log.info("Current work week days {}", Arrays.toString(currentWeekDays.toArray()));
        return currentWeekDays;
    }

    public List<LocalDate> getPreviousDays() {
        List<LocalDate> previousWeekDays = FillerUtils.getPreviousWorkDays();
        previousWeekDays = holidayService.removeHolidays(previousWeekDays);
        previousWeekDays = vacationService.removeVacations(previousWeekDays);
        log.info("Previous work week days {}", Arrays.toString(previousWeekDays.toArray()));
        return previousWeekDays;
    }

    public List<LocalDate> getWorkDays(LocalDate from, LocalDate to) {
        List<LocalDate> currentWeekDays = FillerUtils.getDaysBetween(from, to);
        currentWeekDays = holidayService.removeHolidays(currentWeekDays);
        currentWeekDays = vacationService.removeVacations(currentWeekDays);
        currentWeekDays = currentWeekDays.stream()
                .filter(d -> d.getDayOfWeek() != DayOfWeek.SATURDAY && d.getDayOfWeek() != DayOfWeek.SUNDAY)
                .collect(Collectors.toList());
        return currentWeekDays;
    }

    public List<LocalDate> getVacationDays(LocalDate from, LocalDate to) {
        return vacationService.getVacations().stream().filter(v -> v.isAfter(from) && v.isBefore(to))
                .collect(Collectors.toList());
    }

}
