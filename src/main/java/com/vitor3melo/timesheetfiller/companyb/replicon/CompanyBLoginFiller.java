package com.vitor3melo.timesheetfiller.companyb.replicon;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class CompanyBLoginFiller {

    @Value("${url.login.companyb}")
    private String loginUrl;

    @Value("${credentials.companyb.username}")
    private String username;

    @Value("${credentials.companyb.password}")
    private String password;

    @Autowired
    private WebDriver webDriver;

    public void fillLogin() {
        log.info("Calling companyb login page");
        webDriver.get(loginUrl);

        try {
            log.info("Checking if login page was reached");
            var isLoginPage = new WebDriverWait(this.webDriver, Duration.ofSeconds(2))
                    .until(ExpectedConditions.urlContains("login"));
            if (isLoginPage != null && isLoginPage) {
                log.info("Filling login form");
                WebElement usernameElement = webDriver.findElement(By.id("LoginNameTextBox"));
                WebElement passwordElement = webDriver.findElement(By.id("PasswordTextBox"));
                usernameElement.sendKeys(username);
                passwordElement.sendKeys(password);
                log.info("Clicking login");
                WebElement button = webDriver.findElement(By.id("LoginButton"));
                button.click();
            }
        } catch (TimeoutException e) {
            log.info("Login page was not reached");
        }

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            log.error(e);
        }

    }

}
