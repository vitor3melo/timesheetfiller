package com.vitor3melo.timesheetfiller.companyb.replicon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vitor3melo.timesheetfiller.common.Filler;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class CompanyBMainOrchestrator implements Filler {

    @Autowired
    CompanyBLoginFiller loginFiller;

    @Autowired
    CompanyBTimesheetFiller timesheetFiller;

    @Override
    public void run() {
        try {
            log.info("Filling companyb login");
            loginFiller.fillLogin();
            timesheetFiller.fillTimesheet();
        } catch (Exception e) {
            log.error(e);
        }
    }

}
