package com.vitor3melo.timesheetfiller.companyb.replicon;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.vitor3melo.timesheetfiller.exception.CouldNotGetURLDateException;
import com.vitor3melo.timesheetfiller.service.DateProviderService;
import com.vitor3melo.timesheetfiller.service.VacationService;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class CompanyBTimesheetFiller {

    @Value("${url.timesheet.companyb}")
    private String timesheetUrl;

    @Value("${project.companyb.main}")
    private String workingProject;

    @Value("${project.companyb.specific}")
    private String workingProjectSpecific;

    @Autowired
    private WebDriver webDriver;

    @Autowired
    VacationService vacationService;

    @Autowired
    DateProviderService dateProviderService;

    private int scannedRows = 0;

    private static final String DATE_PARSE_FORMAT = "LLL d, yyyy";

    public void fillTimesheet() {
        int currentYear = LocalDate.now().getYear();

        var interval = checkUnfilledWeeks();
        while (interval != null) {
            var splitInterval = interval.trim().split(" - ");

            LocalDate from = parseDate(splitInterval[0], DATE_PARSE_FORMAT);
            LocalDate to = parseDate(splitInterval[1], DATE_PARSE_FORMAT);

            if (from.getYear() == currentYear && from.getMonth().equals(LocalDate.now().getMonth())) {
                log.info("Trying to fill from {} to {}", from, to);
                fillWeek(from, to);
            } else {
                // only fill if in this year and this month
                log.info("Ignored filling from {} to {}", from, to);
            }

            interval = checkUnfilledWeeks();
        }
    }

    private void clickSubmit(WebElement topButtonsToolbar) {
        var toolbarButtons = topButtonsToolbar.findElements(By.cssSelector("ul.actionList li"));

        for (WebElement webElement : toolbarButtons) {
            if (webElement.getText().equalsIgnoreCase("Submit for Approval")) {
                webElement.click();
                break;
            }
        }
    }

    private void fillWeek(LocalDate from, LocalDate to) {
        var currentDate = webDriver.getCurrentUrl().replaceAll(timesheetUrl + "/", "");
        log.info("Current URL date is: {}", currentDate);

        if (currentDate.isBlank()) {
            throw new CouldNotGetURLDateException("currentDate is blank");
        }

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            log.error(e);
        }

        var timeTable = webDriver.findElement(By.cssSelector("table.dataGrid.dateGrid"));
        var actions = timeTable.findElement(By.cssSelector("tbody[sectiontype=\"actions\"]"));
        var existingRowSection = timeTable.findElement(By.cssSelector("tbody[section=\"0\"][sectiontype=\"rows\"]"));

        var rows = existingRowSection.findElements(By.cssSelector("tr"));

        if (rows == null || rows.isEmpty()) {
            // click action to add a row
            var addActionButton = actions.findElement(By.cssSelector("a#add-new-timeline"));
            addActionButton.click();
        }

        // update the table to not get stale
        existingRowSection = timeTable.findElement(By.cssSelector("tbody[section=\"0\"][sectiontype=\"rows\"]"));

        var row = selectProject(existingRowSection);
        if (row != null) {
            var header = timeTable.findElement(By.cssSelector("thead"));
            WebElement holidayRow = null;
            try {
                holidayRow = timeTable.findElement(By.cssSelector("tbody[section=\"1\"][sectiontype=\"rows\"]"));
            } catch (Exception e) {
                log.info("There's no holiday in this table");
            }
            fillTimeTable(existingRowSection, header, holidayRow, row, from, to);
        }

        var topButtonsToolbar = webDriver.findElement(By.cssSelector(".toolbar"));

        fillVacations(topButtonsToolbar, from, to);

        clickSubmit(topButtonsToolbar);

    }

    private void fillVacations(WebElement topButtonsToolbar, LocalDate from, LocalDate to) {
        var vacations = vacationService.getVacations(from, to);
        log.info("Vacations from {} to {}: {}", from, to, Arrays.toString(vacations.toArray()));
        if (vacations.isEmpty()) {
            return;
        }

        var timeoffButton = topButtonsToolbar
                .findElement(By.cssSelector("ul.actionList li input[value=\"Book Time Off\"]"));
        timeoffButton.click();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            log.error(e);
        }

        var timeoffPopup = webDriver.findElement(By.cssSelector("div.inlinePopup"));
        var timeoffForm = timeoffPopup.findElement(By.cssSelector("div.timeOffBlocks.left"));

        var timeoffType = new Select(timeoffForm.findElement(By.cssSelector("select[id=\"timeOffType\"]")));
        timeoffType.selectByVisibleText("Vacation");

        var timeoffStart = timeoffForm.findElement(By.cssSelector("input[id=\"startDate\"]"));
        timeoffStart.click();
        timeoffStart.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        timeoffStart.sendKeys(vacations.get(0).format(DateTimeFormatter.ofPattern(DATE_PARSE_FORMAT)));
        timeoffStart.click();

        var timeoffEnd = timeoffForm.findElement(By.cssSelector("input[id=\"endDate\"]"));
        timeoffEnd.click();
        timeoffEnd.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));
        timeoffEnd.sendKeys(vacations.get(vacations.size() - 1).format(DateTimeFormatter.ofPattern(DATE_PARSE_FORMAT)));
        timeoffEnd.click();

        var buttonRow = timeoffPopup.findElement(By.cssSelector("div.buttonRow"));
        buttonRow.findElement(By.cssSelector("input[id=timeOffSubmit]")).click();

    }

    private void fillTimeTable(WebElement projectRow, WebElement header, WebElement timeOffTable, String row,
            LocalDate from, LocalDate to) {
        // get row with the days (more reliable this way)
        var dateRow = header.findElements(By.cssSelector("tr th"));
        // get row with inputs
        var inputRow = projectRow.findElements(By.cssSelector("tr." + row + " td"));
        // get row with holidays
        List<WebElement> timeOffRows = null;
        List<WebElement> holidayRowElements = null;
        try {
            timeOffRows = timeOffTable.findElements(By.cssSelector("tr.timeOff"));
            if (timeOffRows != null && !timeOffRows.isEmpty()) {
                var holidayRow = timeOffRows.stream()
                        .filter(r -> r.findElement(By.cssSelector("td:nth-child(1)")).getText()
                                .equalsIgnoreCase("holiday"))
                        .findFirst().get();
                holidayRowElements = holidayRow.findElements(By.cssSelector("td"));
            }
        } catch (NoSuchElementException e) {
            log.info("Holiday row does not exist in this week");
        }
        // tr.timeOff:nth-child(2) > td:nth-child(1)
        if (dateRow.size() != inputRow.size()
                || (holidayRowElements != null && dateRow.size() != holidayRowElements.size())) {
            throw new RuntimeException("Mismatch between element lines in table");
        }

        var workDays = dateProviderService.getWorkDays(from, to).stream()
                .map(d -> String.valueOf(d.getDayOfMonth()))
                .collect(Collectors.toList());
        log.info("Current workdays: {}", Arrays.toString(workDays.toArray()));

        var startIndex = -1;
        var endIndex = -1;
        for (int i = 0; i < dateRow.size(); i++) {
            var hasClassDay = dateRow.get(i).getAttribute("class").contains("day");
            if (hasClassDay && startIndex == -1) {
                startIndex = i;
            }
            if (hasClassDay) {
                endIndex = i;
            }
        }

        for (int i = startIndex; i <= endIndex; i++) {
            var currentColumnDay = dateRow.get(i).getText().replaceAll("[A-Za-z]*", "").trim();
            var input = inputRow.get(i).findElement(By.cssSelector("input"));

            var holidayFilled = false;
            if (holidayRowElements != null && holidayRowElements.size() == inputRow.size()) {
                holidayFilled = holidayRowElements.get(i).findElement(By.cssSelector("div.combinedInput")).getText()
                        .isEmpty();
            }

            if (workDays.contains(currentColumnDay) && !holidayFilled) {
                input.sendKeys("8");
            } else {
                input.clear();
            }

        }

    }

    private String selectProject(WebElement tbodyRows) {
        var taskDropdown = tbodyRows.findElement(By.cssSelector("span.taskSelectorSearchByCategoryContainer a"));
        taskDropdown.click();

        var taskSearchField = tbodyRows.findElement(By.cssSelector("div.taskSelectorSearchField input"));
        taskSearchField.sendKeys(workingProject);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            log.error(e);
        }

        // I hope this never changes place and classes
        var dropDownPopup = webDriver.findElement(By.cssSelector(
                "div.divDropdownContent.multiLevelSelectorContent.selectorWithFavorites"));
        var highlightedOption = dropDownPopup.findElement(By.cssSelector("ul.divDropdownList"));

        if (highlightedOption.getText().toLowerCase().contains(workingProject)) {
            highlightedOption.click();
            // this was updated so fetching a new hook for it
            taskSearchField = tbodyRows.findElement(By.cssSelector("div.taskSelectorSearchField input"));
            taskSearchField.sendKeys(workingProjectSpecific);

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                log.error(e);
            }

            dropDownPopup = webDriver.findElement(By.cssSelector(
                    "div.divDropdownContent.multiLevelSelectorContent.selectorWithFavorites"));

            var projectSpecific = dropDownPopup.findElement(By.cssSelector("ul.divDropdownList"));
            if (projectSpecific.getText().toLowerCase().contains(workingProjectSpecific)) {
                projectSpecific.click();
                return tbodyRows.findElement(By.cssSelector("tr")).getAttribute("class");
            }
            return null;
        }
        return null;
    }

    public String checkUnfilledWeeks() {
        webDriver.get(timesheetUrl);

        // get table rows
        var tableLines = webDriver.findElements(By.cssSelector("#grid tr[id]:not([id=''])"));

        // start from previous row
        for (; scannedRows < tableLines.size(); scannedRows++) {
            var currentRow = tableLines.get(scannedRows);

            var approvalStatusColumn = currentRow.findElement(By.cssSelector("td:nth-child(3)"));
            if (approvalStatusColumn.getText().equalsIgnoreCase("not submitted")) {
                var dateInterval = currentRow.findElement(By.cssSelector("td:nth-child(2)"));
                var intervalString = dateInterval.getText();

                log.info("Date interval ({}) is not submitted, clicking on it", intervalString);
                scannedRows++;
                dateInterval.click();
                return intervalString;
            }
        }
        return null;
    }

    private LocalDate parseDate(String date, String pattern) {
        date = date.replaceAll("\\([A-Za-z]*\\)", "").trim();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return LocalDate.parse(date, formatter);
    }

}
